// BASIC FRAMEWORKS
// ---------------------
var winston = require('winston'),
    util = require('util');

// ceate all transporter
var logger,
    fileTransporter = new (winston.transports.File)({
        level: process.env.LOG_LEVEL_FILE || 'debug',
        filename: global.bots.config.loggerFilename,
        handleExceptions: true,
        //json: true,
        maxsize: 5242880, //5MB
        maxFiles: 5
    }),
    consoleTransporter = new (winston.transports.Console)({
        level: process.env.LOG_LEVEL_CONSOLE || 'debug',
        handleExceptions: true,
        colorize: true,
        inlineMeta: true,
        timestamp: function timestamp() {
            return new Date().toISOString();
        },
        formatter: function formatter(options) {
            // Return string will be passed to logger.
            return winston.config.colorize(options.level, options.level.toUpperCase()) +
                ": [" + options.timestamp() + "]" + " " +
                (options.meta && options.meta.function ? options.meta.function + "\t" : '') +
                (undefined !== options.message ? options.message : '') +
                (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' );
        }
    });

// console.x to winston
function formatArgs(args) {
    return [util.format.apply(util.format, Array.prototype.slice.call(args))];
}

console.log = function log() {
    logger.info.apply(logger, formatArgs(arguments));
};
console.info = function info() {
    logger.info.apply(logger, formatArgs(arguments));
};
console.warn = function warn() {
    logger.warn.apply(logger, formatArgs(arguments));
};
console.error = function error() {
    logger.error.apply(logger, formatArgs(arguments));
};
console.debug = function debug() {
    logger.debug.apply(logger, formatArgs(arguments));
};

logger = new (winston.Logger)({
    exitOnError: false,
    transports: [
        fileTransporter,
        consoleTransporter
    ]
});
// Handle errors
logger.on('error', function (error) {
    console.error(error)
});

module.exports = logger;
module.exports.stream =
{
    write: function write(message, encoding) {
        console.log(message);
    }
};