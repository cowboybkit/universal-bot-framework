// BASIC FRAMEWORKS
// ---------------------
var http = require('http'),
    https = require('https'),
    fs = require('fs'),
    request = require('request');

// MODELS
// ---------------------

// CONTROLLERS
// ---------------------

/**
 * returns the object of an given array of objects that searchs for key == id
 * @param arr array of objects
 * @param id string the search param
 * @param key string the key which should == id [key]
 * @param subkey string the subkey which should == id [key][subkey]
 * @returns {object|boolean} false if not found
 */
exports.getObjectByIdFromArray = function getObjectByIdFromArray(arr, id, key, subkey) {
    if (this.isEmpty(arr))
        return false;
    if (this.isEmpty(key))
        key = "id";
    if (!this.isEmpty(subkey)) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][key][subkey] != null && arr[i][key][subkey].toString() == id) {
                return arr[i];
            }
        }
    } else {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][key] != null && arr[i][key].toString() == id) {
                return arr[i];
            }
        }
    }
    return false;
};

/**
 * returns the index of an given array of objects that searchs for key == id
 * @param arr array of objects
 * @param id string the search param
 * @param key string the key which should == id [key]
 * @param subkey string the subkey which should == id [key][subkey]
 * @returns {number|boolean} false if not found
 */
exports.getIndexWithIdFromArray = function getIndexWithIdFromArray(arr, id, key, subkey) {
    if (this.isEmpty(arr))
        return false;
    if (this.isEmpty(key))
        key = "id";
    if (!this.isEmpty(subkey)) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][key][subkey] != null && arr[i][key][subkey].toString() == id) {
                return i;
            }
        }
    } else {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][key] != null && arr[i][key].toString() == id) {
                return i;
            }
        }
    }
    return false;
};

/**
 * get the length of an assoc array
 * @param dict array assoc array
 * @returns {number} size of array
 */
exports.getDictionarySize = function getDictionarySize(dict) {
    var size = 0, key;
    if (!this.isEmpty(dict)) {
        for (key in dict) {
            if (dict.hasOwnProperty(key)) size++;
        }
    }
    return size;
};

/**
 * checks if the given thing is undefined, empty string or null
 * @param thing object|string|number
 * @returns {boolean}
 */
exports.isEmpty = function isEmpty(thing) {
    return (thing == undefined || thing == null || thing === "" || (typeof thing === "string" && thing == "undefined"));
};

/**
 * print time with specific output and return timestamp
 * @param output string
 * @returns {number}
 */
exports.getTime = function getTime(output) {
    var d = new Date();
    return d.getTime();
};

/**
 * makes a array unique
 * @param array
 * @returns {Array}
 */
exports.arrayUnique = function arrayUnique(array) {
    if (!array) return [];
    var a = array.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }
    return a;
};
/**
 * makes a array of objects unique by key
 * @param array with objects
 * @param key
 * @returns {Array}
 */
exports.arrayUniqueWithKey = function arrayUniqueWithKey(array, key) {
    var a = array.concat();
    a.reverse(); // reverse it to always delete the oldest element
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i][key] === a[j][key])
                a.splice(j--, 1);
        }
    }
    return a.reverse();
};

/**
 * sorts a array with objects by given key
 * @param array with objects
 * @param key
 * @returns array
 */
exports.dynamicSort = function dynamicSort(array, key) {
    var sortOrder = 1;
    if (key[0] === "-") {
        sortOrder = -1;
        key = key.substr(1);
    }
    return array.sort(function (a, b) {
        var result;
        if (key == "created" || key == "date") {
            result = (new Date(a[key]) < new Date(b[key])) ? -1 : (new Date(a[key]) > new Date(b[key])) ? 1 : 0;
            //console.log(new Date(a[key]), " < ", new Date(b[key]), new Date(a[key]) < new Date(b[key]), result);
        } else
            result = (a[key] < b[key]) ? -1 : (a[key] > b[key]) ? 1 : 0;
        return result * sortOrder;
    });
};

/**
 * checks if a date is valid
 * @param d Date
 * @returns {boolean}
 */
exports.isValidDate = function isValidDate(d) {
    if (Object.prototype.toString.call(d) !== "[object Date]")
        return false;
    return !isNaN(d.getTime());
};


/**
 * a and b are javascript Date objects
 */
exports.dateDiffInDays = function dateDiffInDays(a, b) {
    if (!this.isValidDate(a) || !this.isValidDate(b)) {
        return 0; // not valid date
    }
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
};

/**
 * generates a guid
 * @returns {string} guid
 */
exports.guid = function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
};

/**
 * joins keys of an object in an array
 * @param arr array with objects
 * @param key key of subobject to join
 * @param joinSeparator
 * @returns {string}
 */
exports.joinStringOfObjectArray = function joinStringOfObjectArray(arr, key, joinSeparator) {
    var returnString = "";
    var joinArray = [];
    if (this.isEmpty(arr))
        return returnString;
    if (this.isEmpty(key))
        key = "id";
    for (var i in arr) {
        if (arr.hasOwnProperty(i)) {
            if (arr[i][key]) {
                joinArray.push(arr[i][key]);
            }
        }
    }
    return joinArray.join(joinSeparator);
};

/**
 * validates a email
 * @param email string
 * @returns {boolean} true if valid
 */
exports.validateEmail = function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
};

/**
 * validates a username
 * a-z,A-Z,0-9,-_.
 * @param username
 * @returns {boolean} true if valid
 */
exports.validateUsername = function validateUsername(username) {
    var re = /^([a-zA-Z0-9\._-]+)$/gi;
    return re.test(username);
};

/**
 * returns a random number between n1 and n2, n2 must be greater then n1
 * @param n1 int
 * @param n2 int
 * @returns {number} random number
 */
exports.randomNumberBetween = function randomNumberBetween(n1, n2) {
    return Math.floor(Math.random() * ((n2 - n1) + 1) + n1);
};

/**
 * removes all whitespaces
 * @param text string
 * @returns string
 */
exports.killWhiteSpace = function killWhiteSpace(text) {
    return text.replace(/\s/g, '');
};

/**
 * reduces all whitespaces except 1
 * @param text string
 * @returns string
 */
exports.reduceWhiteSpace = function reduceWhiteSpace(text) {
    return text.replace(/\s+/g, ' ');
};

/**
 * is needle in array
 * @param needle
 * @param haystack array
 * @returns {boolean}
 */
exports.inArray = function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
};

/**
 * get all reg ex matches
 * @param string
 * @param regEx
 * @returns {Array}
 */
exports.getAllRegExMatches = function getAllRegExMatches(string, regEx) {
    var m;
    var matches = [];
    do {
        m = regEx.exec(string);
        if (m) {
            matches.push(m[2]);
        }
    } while (m);
    matches = exports.arrayUnique(matches);
    return matches;
};

/**
 * gets all tags from a string, unique
 * @param string
 * @param withoutDiamond deletes the # at beginning
 * @returns {Array}
 */
exports.getTagsFromString = function getTagsFromString(string, withoutDiamond) {
    var tagslistarr = string.match(/#\S+/g);
    tagslistarr = exports.arrayUnique(tagslistarr);
    if (withoutDiamond && tagslistarr.length > 0) {
        for (var i = 0; i < tagslistarr.length; i++) {
            tagslistarr[i] = tagslistarr[i].replace("#", "");
        }
    }
    return tagslistarr;
};

/**
 * returns the IP of the user request
 * @param req
 * @returns string IP
 */
exports.getIP = function getIP(req) {
    var ipString = req.headers['x-forwarded-for'] ||
        (req.connection ? req.connection.remoteAddress : undefined) ||
        (req.socket ? req.socket.remoteAddress : undefined) ||
        (req.connection ? (req.connection.socket ? req.connection.socket.remoteAddress : "") : "");
    if (ipString.length > 0 && ipString != "unknown") {
        var ip = ipString.split(", "); // 172.30.58.218, 172.30.58.218, 127.0.0.1
        ip = ip[0];
        return ip;
    } else {
        return null;
    }
};

/**
 * rescrape a url at facebook to clear the og:cache
 * @param url
 * @param retry
 * @param callbackValue
 * @param callback
 */
exports.rescrapeUrl = function rescrapeUrl(url, retry, callbackValue, callback) {
    //console.log("rescrapeUrl: " + url);
    //var url = 'http://graph.facebook.com/?id=http://www.letsdvel.com/dvels/28639d9b-ca10-4290-af16-befd59ab4f57&scrape=true';
    var options = {
        host: 'graph.facebook.com',
        port: 80,
        path: '/?id=' + url + '&scrape=true',
        method: 'POST'
    };

    var req = http.request(options, function (res) {
        res.setEncoding('utf8');

        var chunkData = "";
        res.on('data', function (chunk) {
            chunkData += chunk;
        });

        res.on('end', function () {
            try {
                var parsedChunk = JSON.parse(chunkData);

                //console.log("scraped " + parsedChunk.url);
                if (parsedChunk.url == undefined) console.log(chunkData);
                if (retry) {
                    setTimeout(function () {
                        exports.rescrapeUrl(url, false, callbackValue, callback)
                    }, 5000);
                } else {
                    if (callback) {
                        callback(callbackValue, chunkData);
                    }
                }
            }
            catch (error) {
                console.error(arguments.callee.name || "rescrapeUrl", error);
                if (callback) {
                    callback(callbackValue, chunkData);
                }
            }
        });
    });

    req.end();
};

/**
 * downloads a image and returns the uploadPath
 * @param url
 * @param callback function(error, uploadImagePath)
 */
exports.downloadImage = function downloadImage(url, callback) {
    if (exports.isEmpty(url)) {
        return callback("url is empty");
    }
    var uploadImagePath = "uploads/" + exports.guid() + ".jpg";
    request({
        followAllRedirects: true,
        url: url,
        encoding: null // binary
    }, function (error, response, body) {
        if (response.statusCode != 200)
            return callback("Error: " + response.statusCode);
        fs.writeFile(uploadImagePath, body, function (error) {
            if (error)
                callback(error);
            callback(null, uploadImagePath);
        })
    });
};

/**
 * downloads a image and returns it as base64
 * @param url
 * @param callback function(error, base64image, response)
 */
exports.getBase64ImageFromUrl = function getBase64ImageFromUrl(url, callback) {
    exports.getBinaryImageFromUrl(url, function (error, binaryImage, response) {
        if (error)
            return callback(error);
        callback(null, binaryImage.toString('base64'), response);
    });
};

/**
 * downloads a image and returns it as binary
 * @param url
 * @param callback function(error, binaryImage, response)
 */
exports.getBinaryImageFromUrl = function getBinaryImageFromUrl(url, callback) {
    if (exports.isEmpty(url)) {
        return callback("url is empty");
    }
    request({
        followAllRedirects: true,
        url: url,
        encoding: null // binary
    }, function (error, response, body) {
        if (response.statusCode != 200)
            return callback("Error: " + response.statusCode);
        callback(null, body, response);
    });
};

/**
 * executes a http request GET
 * @param url
 * @param callback
 */
exports.httpGet = function httpGet(url, callback) {
    var bodyarr = [];
    http.get(url, function (res) {
        res.on('data', function (chunk) {
            bodyarr.push(chunk);
        });
        res.on('end', function () {
            callback(res.statusCode, bodyarr.join('').toString());
        });
    }).on('error', function (e) {
        callback(500, e.message);
    });
};

/**
 * generates a alphanumeric code with given count of digits
 * 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ
 * @param digits count of digits
 * @returns String
 */
exports.randomAlphaNumericCode = function randomAlphaNumericCode(digits) {
    function randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }

    return randomString(digits, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
};

/**
 * Parse all necessary infos from the user agent header and add it as parameter to the request object.
 * @param req
 */
exports.parseUserAgentFromRequest = function parseUserAgentFromRequest(req) {
    var infos = exports.parseInfosFromUserAgent(req.headers['user-agent']);
    // merge by reference
    exports.mergeObjectOneIntoSecond(infos, req);
};

/**
 * parse infos from userAgent
 * @param userAgent
 * @returns {{isMobile: boolean, isBrowser: boolean, appVersion: string, clientApp: null, clientOs: null}}
 */
exports.parseInfosFromUserAgent = function parseInfosFromUserAgent(userAgent) {
    var infos = {
        isMobile: false,
        isBrowser: false,
        appVersion: "0.0.0",
        clientApp: null,
        clientOs: null
    };

    // client OS
    if (userAgent && (userAgent.indexOf("Android") !== -1 || userAgent.indexOf("okhttp") !== -1)) {
        infos.clientOs = "android";
        infos.isMobile = true;
    } else if (userAgent && userAgent.indexOf("iOS") !== -1) {
        infos.clientOs = "ios";
        infos.isMobile = true;
    } else {
        infos.clientOs = "browser";
        infos.isBrowser = true;
    }

    return infos;
};

/**
 * is positive integer
 * @param x
 * @returns {boolean}
 * @private
 */
function __isPositiveInteger(x) {
    // http://stackoverflow.com/a/1019526/11236
    return /^\d+$/.test(x);
}

/**
 * Compare two software version numbers (e.g. 1.7.1)
 * Returns:
 *
 *  0 if they're identical
 *  negative if v1 < v2
 *  positive if v1 > v2
 *  Nan if they in the wrong format
 *
 *  E.g.:
 *
 *  assert(version_number_compare("1.7.1", "1.6.10") > 0);
 *  assert(version_number_compare("1.7.1", "1.7.10") < 0);
 *
 *  "Unit tests": http://jsfiddle.net/ripper234/Xv9WL/28/
 *
 *  Taken from http://stackoverflow.com/a/6832721/11236
 *
 * @param v1
 * @param v2
 * @returns v1 < v2 return -1 | v1 == v2 return 0 | v1 > v2 return 1
 */
exports.compareVersionNumbers = function compareVersionNumbers(v1, v2) {
    var v1parts = v1.split('.');
    var v2parts = v2.split('.');

    // First, validate both numbers are true version numbers
    function validateParts(parts) {
        for (var i = 0; i < parts.length; ++i) {
            if (!__isPositiveInteger(parts[i])) {
                return false;
            }
        }
        return true;
    }

    if (!validateParts(v1parts) || !validateParts(v2parts)) {
        return NaN;
    }

    for (var i = 0; i < v1parts.length; ++i) {
        if (v2parts.length === i) {
            return 1;
        }

        if (v1parts[i] === v2parts[i]) {
            continue;
        }
        if (v1parts[i] > v2parts[i]) {
            return 1;
        }
        return -1;
    }

    if (v1parts.length != v2parts.length) {
        return -1;
    }

    return 0;
};

/**
 * Get number from object.
 * @param object
 * @returns {*}
 */
exports.getNumber = function getNumber(object) {
    if (object) {
        return object * 1;
    }

    return null;
};

/**
 * merge object one into second object by reference
 * @param obj1 source
 * @param obj2 destination
 */
exports.mergeObjectOneIntoSecond = function mergeObjectOneIntoSecond(obj1, obj2) {
    if (obj1 && obj2)
        for (var attrname in obj1) {
            if (obj1.hasOwnProperty(attrname))
                obj2[attrname] = obj1[attrname];
        }
};

/**
 * splits a given string object with properties into object array
 * specially for categories usecase
 * @param stringArrayWithProp must look like:
 * [
 *  { key: "", concatedString: "o1,o2,o3" },
 *  ...
 * ]
 * @param splitChar how to split the string default: ","
 *
 * @return array
 * [
 *  { slug: concatedString[x].slug, name: concatedString[x].name, color: concatedString[x].color },
 *  ...
 * ]
 */
exports.splitStringIntoObjectArray = function splitStringIntoObjectArray(stringArrayWithProp, splitChar) {
    if (!stringArrayWithProp || typeof stringArrayWithProp !== "object")
        return [];
    if (!splitChar) splitChar = ",";
    var splittedArray = {}, objectArray = [];
    stringArrayWithProp.forEach(function (stringObject) {
        if (stringObject.concatedString)
            splittedArray[stringObject.key] = stringObject.concatedString.split(splitChar);
    });
    for (var key in splittedArray) {
        if (!splittedArray.hasOwnProperty(key))
            continue;
        var obj = splittedArray[key];
        for (var index = 0; index < obj.length; index++) {
            if (typeof objectArray[index] != "object") {
                objectArray[index] = {};
            }
            objectArray[index][key] = obj[index];
        }
    }
    return objectArray;
};

/**
 * sleep
 * @param sleepDuration in ms
 */
exports.sleepFor = function sleepFor(sleepDuration) {
    var now = new Date().getTime();
    while (new Date().getTime() < now + sleepDuration) {
        /* do nothing, waiting */
    }
};