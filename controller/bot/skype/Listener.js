// BASIC FRAMEWORKS
// ---------------------
var SkypeSDK = require('./skype-sdk');

// MODELS
// ---------------------
var model = require('../../../model');

// CONTROLLERS
// ---------------------
var util = require("../../../util");
var controller = require("../../../controller");

// CONST
// ---------------------
var constant = require("../../../constant");
var _chatBotPlatform = "Skype"; // case-sensitive, must be the same as in config.bot.json and bot/index.js

// CONFIG
// ---------------------
var config = require("../../../config/config.bot")[global.bots.stageToUse][_chatBotPlatform];

exports.isEnabled = function isEnabled() {
    return !util.Helpers.isEmpty(config.applicationId);
};
if (!exports.isEnabled()) {
    console.warn("Not initializing Skype bot");
    return;
}

// Initialize Bot
var bot = new SkypeSDK.BotService({
    messaging: {
        botId: config.botId,
        serverUrl: "https://apis.skype.com",
        requestTimeout: 15000,
        appId: config.applicationId,
        appSecret: config.password
    }
});

bot.SkypeSDK = SkypeSDK;

bot.getMessagingHandler = function getMessagingHandler() {
    return SkypeSDK.messagingHandler(bot);
};

bot.on('contactAdded', function (bot, incoming) {
    console.log("contactAdded", bot, incoming);

    bot.send(incoming.from, "nothing implemented yet", true, function (error, result) {
        console.log(error, result);
    });
    //__processMessage(incoming.from.id, {action: text}, {}); // TODO activate it
});

bot.on('personalMessage', function (bot, incoming) {
    console.log("personalMessage", bot, incoming);

    bot.send(incoming.from, "nothing implemented yet", true, function (error, result) {
        console.log(error, result);
    });
    //__processMessage(incoming.from.id, {action: text}, {}); // TODO activate it
});

/**
 * this is the first method after the webhook is called
 * it starts all the rest, session, processing, ...
 * @param chatBotUserId
 * @param payload object {action: ""}
 * @param req
 * @private
 */
function __processMessage(chatBotUserId, payload, req) {
    console.log("__processMessage", chatBotUserId, payload);
    controller.Bot.Handler.processMessage(
        _chatBotPlatform, // chatBotPlatform
        chatBotUserId, // chatBotUserId
        payload, // payload
        req, // req
        function (error, response) {
            console.log("processMessage:after", error, response);
        }
    );
}

bot.isEnabled = exports.isEnabled;
module.exports = bot;